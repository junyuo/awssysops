aws cloudwatch set-alarm-state --alarm-name "CPUUtilizationTerminateInstance" --state-reason "Testing" --state-value ALARM

aws cloudwatch set-alarm-state --alarm-name "TerminateEC2OnHighCPU" --state-reason "Testing" --state-value ALARM

https://docs.aws.amazon.com/cli/latest/reference/cloudwatch/set-alarm-state.html