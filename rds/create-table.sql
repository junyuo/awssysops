CREATE TABLE films (
    code        char(5),
    title       varchar(40) NOT NULL,
    did         integer NOT NULL,
    date_prod   date,
    kind        varchar(10),
    len         varchar(40),
    CONSTRAINT firstkey PRIMARY KEY (code)
);

INSERT INTO films VALUES
    ('UA502', 'Bananas', 105, '1971-07-13', 'Comedy', '82 minutes');