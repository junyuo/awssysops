# generate root access keys
aws configure --profile root-mfa-delete-demo

# enable mfa delete
# syntax
aws s3api put-bucket-versioning --bucket mfa-demo-stephane --versioning-configuration Status=Enabled,MFADelete=Enabled --mfa "arn-of-mfa-device mfa-code" --profile root-mfa-delete-demo
# example
aws s3api put-bucket-versioning --bucket albert-mfa-delete-demo --versioning-configuration Status=Enabled,MFADelete=Enabled --mfa "arn:aws:iam::346078327502:mfa/albert_kuo	692930" --profile root-mfa-delete-demo


# disable mfa delete
#syntax
aws s3api put-bucket-versioning --bucket mfa-demo-stephane --versioning-configuration Status=Enabled,MFADelete=Disabled --mfa "arn-of-mfa-device mfa-code" --profile root-mfa-delete-demo
#example
aws s3api put-bucket-versioning --bucket mfa-demo-stephane --versioning-configuration Status=Enabled,MFADelete=Disabled --mfa "arn:aws:iam::346078327502:mfa/albert_kuo 123324" --profile root-mfa-delete-demo

# delete the root credentials in the IAM console!!!